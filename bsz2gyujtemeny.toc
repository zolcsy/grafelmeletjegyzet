\contentsline {section}{\numberline {1}Alapfogalmak}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}R\IeC {\'e}szgr\IeC {\'a}fok \IeC {\'e}s Izomorfia}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}F\IeC {\'a}k}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}S\IeC {\'\i }kbarajzolhat\IeC {\'o}s\IeC {\'a}g}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Du\IeC {\'a}lis}{7}{subsection.2.1}
\contentsline {section}{\numberline {3}Euler \IeC {\'e}s Hamilton k\IeC {\"o}r\IeC {\"o}k}{8}{section.3}
\contentsline {section}{\numberline {4}Gr\IeC {\'a}fok kromatikus sz\IeC {\'a}ma}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Intervallumgr\IeC {\'a}fok}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}\IeC {\'E}lkromatikus sz\IeC {\'a}m}{12}{subsection.4.2}
\contentsline {section}{\numberline {5}Nevezetes cs\IeC {\'u}cs- \IeC {\'e}s \IeC {\'e}lhalmazok}{13}{section.5}
\contentsline {subsection}{\numberline {5.1}P\IeC {\'a}ros\IeC {\'\i }t\IeC {\'a}sok}{14}{subsection.5.1}
\contentsline {section}{\numberline {6}H\IeC {\'a}l\IeC {\'o}zati folyamok}{16}{section.6}
\contentsline {subsection}{\numberline {6.1}H\IeC {\'a}l\IeC {\'o}zat \IeC {\'a}ltal\IeC {\'a}nos\IeC {\'\i }t\IeC {\'a}s}{17}{subsection.6.1}
\contentsline {section}{\numberline {7}T\IeC {\"o}bbsz\IeC {\"o}r\IeC {\"o}s \IeC {\"o}sszef\IeC {\"u}gg\IeC {\H o}s\IeC {\'e}g}{18}{section.7}
\contentsline {subsection}{\numberline {7.1}Menger-t\IeC {\'e}telek}{18}{subsection.7.1}
\contentsline {section}{\numberline {8}\IeC {\'A}ltal\IeC {\'a}nos algoritmusok}{20}{section.8}
\contentsline {subsection}{\numberline {8.1}Sz\IeC {\'e}less\IeC {\'e}gi keres\IeC {\'e}s}{20}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Minim\IeC {\'a}lis fesz\IeC {\'\i }t\IeC {\H o}fa}{20}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Gr\IeC {\'a}f reprezent\IeC {\'a}ci\IeC {\'o}}{21}{subsection.8.3}
\contentsline {section}{\numberline {9}Legr\IeC {\"o}videbb \IeC {\'u}t keres\IeC {\'e}se}{22}{section.9}
\contentsline {subsection}{\numberline {9.1}Ford \IeC {\'e}s Floyd algoritmusa}{22}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Dijkstra algoritmusa}{22}{subsection.9.2}
\contentsline {section}{T\IeC {\'a}rgymutat\IeC {\'o}}{23}{subsection.9.2}
\contentsline {section}{Hivatkoz\IeC {\'a}sok}{25}{section*.2}
\contentsline {section}{Jegyzet}{25}{section*.3}
